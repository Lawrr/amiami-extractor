#!/usr/bin/env python3

import json
import logging.config
import sys

import config
from amiami_extractor import constants
from amiami_extractor.extractor import Extractor


def run(**kwargs):
    override_constants()
    setup_logging(config.LOGGING_CONFIG_PATH)

    extractor = Extractor(db_path=config.DB_PATH, **kwargs)
    try:
        extractor.run(config.QUERY_URLS, config.FIRST_RUN_MAX_QUERY_PAGES, config.MAX_QUERY_PAGES,
                      config.NUM_UPDATE_RECENT, config.NUM_UPDATE_OLD)
    except KeyboardInterrupt:
        sys.exit()
    except Exception as e:
        extractor.logger.exception(repr(e))
    finally:
        extractor.close()


def override_constants():
    constants.IMAGES_PATH = config.IMAGES_PATH


def setup_logging(config_path):
    with open(config_path, 'r') as f:
        config = json.load(f)
    logging.config.dictConfig(config)
