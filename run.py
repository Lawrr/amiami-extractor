#!/usr/bin/env python3

import click

import app


@click.command()
@click.option('--update', '-u', is_flag=True, default=False, help='Update existing items on top of getting new items')
@click.option('--backfill', is_flag=True, default=False, help='Sets insert date for items to epoch time zero')
@click.option('--full-run', is_flag=True, default=False, help='Will not early exit even if all the items on the page have been seen before')
def run(update, backfill, full_run):
    app.run(update=update, backfill=backfill, full_run=full_run)


if __name__ == '__main__':
    run()
