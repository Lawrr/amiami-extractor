import re


def clean_filename(path):
    return re.sub(r'[<>:"/\\|?*]', '', path)
