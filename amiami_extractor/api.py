import concurrent.futures
import asyncio
from collections import OrderedDict
import functools
import logging
from urllib.parse import parse_qs, urlparse
import time

import requests

MAX_CONCURRENT_REQUESTS = 25

OVERRIDDEN_SEARCH_ITEMS_URL_PARAMS = ['pagemax', 'pagecnt', 's_sortkey', 'lang']

ITEMS_PER_PAGE = 20
SORT_KEY = 'regtimed'

SEARCH_ITEMS_API_URL = 'https://api.amiami.com/api/v1.0/items'
ITEM_DETAILS_API_URL = 'https://api.amiami.com/api/v1.0/item'
IMAGES_URL = 'https://img.amiami.com'


def _get_api_headers():
    return {'x-user-key': 'amiami_dev'}


def _get_required_search_items_params(page_number):
    return {
        'pagemax': ITEMS_PER_PAGE,
        'pagecnt': page_number,
        's_sortkey': SORT_KEY,
        'lang': 'eng',
        'age_confirm': 'true',
    }


def get_search_items_response(url, page_number):
    params = _get_remaining_params(url, OVERRIDDEN_SEARCH_ITEMS_URL_PARAMS)
    params.update(_get_required_search_items_params(page_number))
    response = requests.get(SEARCH_ITEMS_API_URL, params=params, headers=_get_api_headers(), timeout=20)

    if response.status_code != 200:
        raise RuntimeError('Failed to fetch data. status_code={} page_number={} url={{{}}}'.format(
            response.status_code, page_number, response.url))

    return response


def _get_remaining_params(url, overridden_params):
    """Remove params from url and then returns the remaining params."""
    parsed_url = urlparse(url)
    query = OrderedDict(sorted(parse_qs(parsed_url.query).items()))

    for key in overridden_params:
        query.pop(key, None)

    return query


def get_item_details_responses(gcodes, use_scode=False):
    logger = logging.getLogger(__name__)
    gcodes = gcodes.copy()
    loop = asyncio.get_event_loop()

    responses = {}
    concurrent_gcodes = []
    while len(gcodes) > 0:
        concurrent_gcodes.append(gcodes.pop())

        if len(gcodes) == 0 or len(concurrent_gcodes) == MAX_CONCURRENT_REQUESTS:
            logger.info('Getting item details. gcodes={{{}}}'.format(concurrent_gcodes))
            tries_remaining = 5
            while tries_remaining > 0:
                tries_remaining -= 1
                try:
                    responses.update(loop.run_until_complete(_get_item_details_responses(concurrent_gcodes, use_scode=use_scode)))
                    break
                except (requests.exceptions.ConnectionError, requests.exceptions.ReadTimeout):
                    logger.warning('Error occurred while requesting item details - retrying. tries_remaining={}'.format(tries_remaining))
                    time.sleep(10)
                    continue

            concurrent_gcodes = []
            time.sleep(1)

    return responses


@asyncio.coroutine
def _get_item_details_responses(gcodes, use_scode):
    responses = {}
    futures = {}
    loop = asyncio.get_event_loop()
    with concurrent.futures.ThreadPoolExecutor(max_workers=len(gcodes)) as pool:
        for gcode in gcodes:
            params = {
                'lang': 'eng'
            }
            if use_scode:
                params['scode'] = gcode
            else:
                params['gcode'] = gcode

            func = functools.partial(requests.get, ITEM_DETAILS_API_URL, params=params, headers=_get_api_headers(), timeout=20)
            future = loop.run_in_executor(pool, func)
            futures[gcode] = future

        for gcode, future in futures.items():
            response = yield from future
            responses[gcode] = response

    return responses


def get_item_thumbnail_responses(items):
    logger = logging.getLogger(__name__)
    items = items.copy()
    loop = asyncio.get_event_loop()

    responses = {}
    concurrent_items = []
    while len(items) > 0:
        concurrent_items.append(items.pop())

        if len(items) == 0 or len(concurrent_items) == MAX_CONCURRENT_REQUESTS:
            logger.info('Getting item thumbnails. gcodes={{{}}}'.format([item.gcode for item in concurrent_items]))
            tries_remaining = 5
            while tries_remaining > 0:
                tries_remaining -= 1
                try:
                    responses.update(loop.run_until_complete(_get_item_thumbnail_responses(concurrent_items)))
                    break
                except (requests.exceptions.ConnectionError, requests.exceptions.ReadTimeout):
                    logger.warning('Error occurred while requesting item thumbnails - retrying. tries_remaining={}'.format(tries_remaining))
                    time.sleep(10)
                    continue

            concurrent_items = []
            time.sleep(1)

    return responses


@asyncio.coroutine
def _get_item_thumbnail_responses(items):
    responses = {}
    futures = {}
    loop = asyncio.get_event_loop()
    with concurrent.futures.ThreadPoolExecutor(max_workers=len(items)) as pool:
        for item in items:
            func = functools.partial(requests.get, item.thumbnail_url, timeout=20)
            future = loop.run_in_executor(pool, func)
            futures[item.gcode] = future

        for gcode, future in futures.items():
            response = yield from future
            responses[gcode] = response

    return responses
