import copy
import datetime as dt
import logging
import mimetypes

from . import api, parser


def get_new_items_from_url(url, start_page, end_page, db_manager, full_run):
    logger = logging.getLogger(__name__)

    new_items = set()
    unchanged_items = set()
    for page_number in range(start_page, end_page + 1):
        logger.info('Processing url. url={{{}}} page_number={}'.format(url, page_number))

        response = api.get_search_items_response(url, page_number)
        page_items = parser.parse_search_items_json(response.json())

        new_page_items, unchanged_page_items = _get_changed_items_with_details(page_items, db_manager)
        _add_item_thumbnails(new_page_items)

        new_items.update(new_page_items)
        unchanged_items.update(unchanged_page_items)

        logger.info('Found {} new products on page'.format(len(page_items)))

        if len(page_items) < api.ITEMS_PER_PAGE:
            logger.info('page_items < api.ITEMS_PER_PAGE. url={} page_number={} page_items={}'.format(url, page_number, len(page_items)))
            break
        elif not full_run and len(new_page_items) == 0:
            logger.info('new_page_items == 0. url={} page_number={}'.format(url, page_number))
            break

    logger.info('Getting other items. url={{{}}}'.format(url))
    new_other_items, unchanged_other_items = _get_other_items(new_items, db_manager)
    new_items.update(new_other_items)
    unchanged_items.update(unchanged_other_items)

    return new_items, unchanged_items


def get_updated_items_from_least_recently_checked(db_manager, max):
    logger = logging.getLogger(__name__)

    items = db_manager.get_least_recently_checked_items(max)

    logger.info('Updating least recently checked items. item_count={}'.format(len(items)))

    new_items, unchanged_items = _get_changed_items_with_details(items, db_manager, use_scode=True)
    return new_items, unchanged_items


def get_updated_items_within_datetimes(start, end, db_manager, max=0):
    # Note: max param is included so we can reduce the number of requests we send to the server
    logger = logging.getLogger(__name__)

    items = db_manager.get_items_within_datetimes(start, end)

    if max > 0:
        items = items[:max]

    logger.info('Updating recently added items. start={} end={} item_count={}'.format(start, end, len(items)))

    new_items, unchanged_items = _get_changed_items_with_details(items, db_manager, use_scode=True)
    return new_items, unchanged_items


def _get_other_items(items, db_manager):
    other_items = []
    for item in items:
        item_other_items = parser.parse_other_items(item)
        other_items.extend(item_other_items)

    new_other_items, unchanged_other_items = _get_changed_items_with_details(other_items, db_manager, use_scode=True)
    return new_other_items, unchanged_other_items


def _add_item_thumbnails(items):
    logger = logging.getLogger(__name__)

    thumbnail_items = list(filter(lambda item: item.thumbnail_url is not None and not item.in_database, items))
    responses = api.get_item_thumbnail_responses(thumbnail_items)
    for item in thumbnail_items:
        response = responses[item.gcode]

        if response.status_code != 200:
            logger.error('Failed to fetch thumbnail. status_code={} url={{{}}} gcode={}'.format(
                response.status_code, response.url, item.gcode))
            continue

        extensions = mimetypes.guess_all_extensions(response.headers.get('content-type'))
        if '.jpg' in extensions:
            extension = '.jpg'
        elif '.png' in extensions:
            extension = '.png'
        elif '.gif' in extensions:
            extension = '.gif'
        else:
            raise RuntimeError('Invalid thumbnail extensions. gcode={} extensions={{{}}}'.format(item.gcode, extensions))

        item.thumbnail = response.content
        item.thumbnail_extension = extension.lstrip('.')


def _get_changed_items_with_details(items, db_manager, use_scode=False):
    logger = logging.getLogger(__name__)
    items = copy.deepcopy(items)

    changed_items = set()
    responses = api.get_item_details_responses([item.gcode for item in items], use_scode=use_scode)
    for item in items:
        response = responses[item.gcode]
        if response.status_code == 400 and use_scode:
            # Usually means out of stock
            latest_item = db_manager.get_latest_item(item.id, item.type, item.gcode_extra)
            if latest_item is None:
                continue
            item.set(**latest_item.__dict__)
            item.set(stock=0)
        elif response.status_code != 200:
            logger.warn('Failed to fetch data. status_code={} url={{{}}} response={{{}}}'.format(response.status_code,
                                                                                                 response.url,
                                                                                                 response.json()))
            continue
        else:  # Status code 200
            try:
                item_details = parser.parse_item_details_json(response.json())
            except KeyError as e:
                logger.warn('KeyError while parsing item details. url={{{}}} error={{{}}}.\n'.format(response.url, repr(e)))
                continue
            except Exception as e:
                # TODO handle sold out salestatus_detail
                logger.error('Misc exception while parsing item details. url={{{}}} error={{{}}}.\n'.format(response.url, repr(e)))
                continue

            # Override gcode with scode
            id, type, gcode_extra, gcode_length = parser.parse_gcode(item_details['scode'])

            item.set(
                gcode_extra=gcode_extra,
                list_price=item_details['list_price'],
                price=item_details['price'],
                item_condition=item_details['item_condition'],
                box_condition=item_details['box_condition'],
                release_date=item_details['release_date'],
                sale_status=item_details['sale_status'],
                stock=item_details['stock'],
                other_items=item_details['other_items']
            )

        # Remove non-changed items
        latest_item = db_manager.get_latest_item(item.id, item.type, item.gcode_extra)

        if latest_item is not None and latest_item.stock != item.stock:
            logger.debug('Stock has changed since last seen. gcode={} old_stock={} new_stock={}'.format(
                item.gcode, latest_item.stock, item.stock))

        # If item/box condition is now None it probably means it is now out of stock
        if latest_item is not None and has_condition_nulled(latest_item, item):
            logger.info('Condition is now None. Probably now out of stock. gcode={}'.format(item.gcode))
            item.set(
                item_condition=latest_item.item_condition,
                box_condition=latest_item.box_condition,
                stock=0
            )

        if latest_item is not None and not has_item_changed(latest_item, item):
            logger.info('Skipping - Item has not changed since last seen. gcode={}'.format(item.gcode))
            continue
        else:
            logger.info('Item changed. gcode={} latest_item={{{}}} new_item={{{}}}'.format(item.gcode, latest_item, item))

        item.set(in_database=(latest_item is not None))
        changed_items.add(item)

    unchanged_items = set(items) - changed_items

    return changed_items, unchanged_items


def has_condition_nulled(old, new):
    return (
        (old.item_condition is not None and old.box_condition is not None)
        and
        (new.item_condition is None or new.box_condition is None)
    )


def has_item_changed(old, new):
    return (
        old.price != new.price or
        old.item_condition != new.item_condition or
        old.box_condition != new.box_condition or
        old.release_date != new.release_date or
        old.sale_status != new.sale_status or
        ((old.stock == 0) != (new.stock == 0))
    )
