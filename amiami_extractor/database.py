import datetime as dt
import logging
import sqlite3
import time

from . import models

DB_DATE_FORMAT = '%Y-%m-%d %H:%M:%S'


def datetime_to_db_format(dt):
    return dt.strftime(DB_DATE_FORMAT)


class DatabaseManager:
    def __init__(self, db_path, logging_level=logging.DEBUG):
        self.db_path = db_path
        self.conn = sqlite3.connect(self.db_path)
        self.logger = logging.getLogger(__name__)

        self.init_db()

    def close(self):
        self.conn.close()

    def is_first_run(self):
        cursor = self.conn.cursor()
        try:
            cursor.execute("SELECT COUNT(*) FROM items")
            is_first_run = (cursor.fetchone()[0] == 0)
        except sqlite3.OperationalError:
            is_first_run = True
        cursor.close()
        return is_first_run

    def commit(self):
        self.conn.commit()

    def init_db(self):
        cursor = self.conn.cursor()

        cursor.execute('''
            CREATE TABLE IF NOT EXISTS item_types(
                id INTEGER PRIMARY KEY,
                name TEXT
            )''')

        # TODO use single int as primary key?
        cursor.execute('''
            CREATE TABLE IF NOT EXISTS items(
                id INTEGER,
                type INTEGER,
                gcode_length INTEGER,
                name TEXT,
                list_price INTEGER,
                thumbnail_extension TEXT,
                FOREIGN KEY (type) REFERENCES item_types(id),
                PRIMARY KEY (id, type)
            )''')

        cursor.execute('''
            CREATE TABLE IF NOT EXISTS item_scodes(
                id INTEGER,
                type INTEGER,
                gcode_extra TEXT,
                last_checked TEXT,
                FOREIGN KEY (id, type) REFERENCES items(id, type)
                PRIMARY KEY (id, type, gcode_extra)
            )''')
        cursor.execute('''
            CREATE INDEX IF NOT EXISTS item_scodes_last_checked_idx ON item_scodes (last_checked)
            ''')

        cursor.execute('''
            CREATE TABLE IF NOT EXISTS item_snapshots(
                id INTEGER,
                type INTEGER,
                gcode_extra TEXT,
                price INTEGER,
                item_condition INTEGER,
                box_condition INTEGER,
                release_date TEXT,
                sale_status INTEGER,
                stock INTEGER,
                date TEXT,
                FOREIGN KEY (id, type, gcode_extra) REFERENCES item_scodes(id, type, gcode_extra),
                FOREIGN KEY (item_condition) REFERENCES item_conditions(id),
                FOREIGN KEY (box_condition) REFERENCES box_conditions(id),
                FOREIGN KEY (sale_status) REFERENCES sale_statuses(id),
                PRIMARY KEY (id, type, gcode_extra, date)
            )''')

        cursor.execute('''
            CREATE TABLE IF NOT EXISTS item_conditions(
                id INTEGER PRIMARY KEY,
                name TEXT,
                UNIQUE (name)
            )''')
        cursor.executemany('INSERT OR IGNORE INTO item_conditions (name) VALUES (?)',
                           [(condition.value,) for condition in models.ItemCondition])
        cursor.execute('''
            CREATE TABLE IF NOT EXISTS box_conditions(
                id INTEGER PRIMARY KEY,
                name TEXT,
                UNIQUE (name)
            )''')
        cursor.executemany('INSERT OR IGNORE INTO box_conditions (name) VALUES (?)',
                           [(condition.value,) for condition in models.BoxCondition])
        cursor.execute('''
            CREATE TABLE IF NOT EXISTS sale_statuses(
                id INTEGER PRIMARY KEY,
                name TEXT,
                UNIQUE (name)
            )''')
        cursor.executemany('INSERT OR IGNORE INTO sale_statuses (name) VALUES (?)',
                           [(status.value,) for status in models.SaleStatus])

        cursor.execute('SELECT id, name FROM item_types')
        models.item_types = {name: id for id, name in cursor.fetchall()}

        cursor.execute('SELECT id, name FROM item_conditions')
        models.item_conditions = {name: id for id, name in cursor.fetchall()}

        cursor.execute('SELECT id, name FROM box_conditions')
        models.box_conditions = {name: id for id, name in cursor.fetchall()}

        cursor.execute('SELECT id, name FROM sale_statuses')
        models.sale_statuses = {name: id for id, name in cursor.fetchall()}

        cursor.close()
        self.conn.commit()

    def get_least_recently_checked_items(self, max):
        """This query will only get items which have at least 1 snapshot."""
        s = time.time()
        cursor = self.conn.cursor()
        cursor.execute('''
            SELECT snapshot.id, type.name, snapshot.gcode_extra, gcode_length, items.name, items.list_price, snapshot.price, ic.name, bc.name, snapshot.release_date, ss.name, snapshot.stock
            FROM (
                SELECT scode.id, scode.type, scode.gcode_extra, last_checked
                FROM item_scodes AS scode
                WHERE EXISTS (
                    SELECT 1
                    FROM item_snapshots AS snapshot
                    WHERE scode.id=snapshot.id AND scode.type=snapshot.type AND scode.gcode_extra=snapshot.gcode_extra
                    LIMIT 1
                )
                ORDER BY last_checked
                LIMIT ?
            ) AS scode
                JOIN item_snapshots AS snapshot ON scode.id=snapshot.id AND scode.type=snapshot.type AND scode.gcode_extra=snapshot.gcode_extra
                JOIN items ON items.id=snapshot.id AND items.type=snapshot.type
                LEFT OUTER JOIN item_conditions AS ic ON ic.id=snapshot.item_condition
                LEFT OUTER JOIN box_conditions AS bc ON bc.id=snapshot.box_condition
                LEFT OUTER JOIN sale_statuses AS ss ON ss.id=snapshot.sale_status
                JOIN item_types AS type ON type.id=snapshot.type
            WHERE snapshot.date = (
                SELECT max(date)
                FROM item_snapshots AS max_snap
                WHERE max_snap.id=snapshot.id AND max_snap.type=snapshot.type AND max_snap.gcode_extra=snapshot.gcode_extra
            )
        ''', (max,))
        rows = cursor.fetchall()
        self.logger.debug('get_least_recently_checked_items took {:.2f} seconds'.format(time.time() - s))

        items = []
        for id, type, gcode_extra, gcode_length, name, list_price, price, ic, bc, release_date, sale_status, stock in rows:
            model = models.Item(id, type, gcode_extra, gcode_length, name)
            model.set(
                list_price=list_price,
                price=price,
                ic=ic,
                bc=bc,
                release_date=release_date,
                sale_status=sale_status,
                stock=stock
            )
            items.append(model)

        return items

    def get_items_within_datetimes(self, start, end):
        s = time.time()
        cursor = self.conn.cursor()
        cursor.execute('''
            SELECT snapshot.id, type.name, snapshot.gcode_extra, gcode_length, items.name, items.list_price, snapshot.price, ic.name, bc.name, snapshot.release_date, ss.name, snapshot.stock
            FROM item_snapshots AS snapshot
                JOIN item_scodes AS scode ON scode.id=snapshot.id AND scode.type=snapshot.type AND scode.gcode_extra=snapshot.gcode_extra
                JOIN items ON items.id=snapshot.id AND items.type=snapshot.type
                LEFT OUTER JOIN item_conditions AS ic ON ic.id=snapshot.item_condition
                LEFT OUTER JOIN box_conditions AS bc ON bc.id=snapshot.box_condition
                LEFT OUTER JOIN sale_statuses AS ss ON ss.id=snapshot.sale_status
                LEFT OUTER JOIN item_snapshots AS prev_snapshot ON prev_snapshot.id=snapshot.id AND prev_snapshot.type=snapshot.type AND prev_snapshot.gcode_extra=snapshot.gcode_extra AND datetime(prev_snapshot.date)<datetime(snapshot.date)
                JOIN item_types AS type ON type.id=snapshot.type
            WHERE datetime(snapshot.date) >= datetime(?) AND datetime(snapshot.date) < datetime(?)
                AND (datetime(prev_snapshot.date) = (
                    SELECT max(date)
                    FROM item_snapshots np
                    WHERE np.id=snapshot.id AND np.type=snapshot.type AND np.gcode_extra=snapshot.gcode_extra
                        AND datetime(np.date) < datetime(snapshot.date)
                    ) OR prev_snapshot.id IS NULL
                )
            GROUP BY snapshot.id, type.id, snapshot.gcode_extra
            ORDER BY last_checked ASC
        ''', (start, end))
        rows = cursor.fetchall()
        self.logger.debug('get_items_within_datetimes took {:.2f} seconds'.format(time.time() - s))

        items = []
        for id, type, gcode_extra, gcode_length, name, list_price, price, ic, bc, release_date, sale_status, stock in rows:
            model = models.Item(id, type, gcode_extra, gcode_length, name)
            model.set(
                list_price=list_price,
                price=price,
                ic=ic,
                bc=bc,
                release_date=release_date,
                sale_status=sale_status,
                stock=stock
            )
            items.append(model)

        return items

    def get_latest_item(self, id, type, gcode_extra):
        type_id = models.item_types.get(type)

        if type_id is None:
            # type_id is not in item_types so the item can't exist in the db
            return None

        cursor = self.conn.cursor()
        cursor.execute('''
            SELECT i.gcode_length, i.name, s.price, ic.name, bc.name, s.release_date, ss.name, s.stock
            FROM item_snapshots s
            LEFT JOIN items i ON s.id=i.id AND s.type=i.type
            LEFT JOIN item_conditions ic ON s.item_condition=ic.id
            LEFT JOIN box_conditions bc ON s.box_condition=bc.id
            LEFT JOIN sale_statuses ss ON s.sale_status=ss.id
            WHERE s.id=? AND s.type=? AND s.gcode_extra=?
            ORDER BY s.date DESC
            LIMIT 1
        ''', (id, type_id, gcode_extra))
        row = cursor.fetchone()

        item = None
        if row is not None:
            gcode_length, name, price, item_condition, box_condition, release_date, sale_status, stock = row

            item = models.Item(id, type, gcode_extra, gcode_length, name)
            item.price = price
            item.item_condition = item_condition
            item.box_condition = box_condition
            item.release_date = release_date
            item.sale_status = sale_status
            item.stock = stock

        return item

    def get_or_insert_item_type(self, type_name):
        if type_name not in models.item_types.keys():
            cursor = self.conn.cursor()
            cursor.execute('INSERT OR IGNORE INTO item_types (name) VALUES (?)', (type_name,))
            type_id = cursor.lastrowid
            models.item_types[type_name] = type_id
            self.logger.info('Inserted new item type. type={} id={}'.format(type_name, type_id))
            cursor.close()
        type_id = models.item_types[type_name]
        return type_id

    def get_insert_datetime(self, backfill=False):
        if backfill:
            return datetime_to_db_format(dt.datetime.fromtimestamp(0))
        else:
            return datetime_to_db_format(dt.datetime.now())

    def insert_or_update_item_scode(self, id, type, gcode_extra, insert_datetime=None, backfill=False):
        cursor = self.conn.cursor()

        type_id = self.get_or_insert_item_type(type)

        if backfill or insert_datetime is None:
            insert_datetime = self.get_insert_datetime(backfill=backfill)

        item_scodes_entry = (id, type_id, gcode_extra, insert_datetime)
        cursor.execute('''
            INSERT OR REPLACE INTO item_scodes (
                id, type, gcode_extra, last_checked
            )
            VALUES (?, ?, ?, ?)
        ''', item_scodes_entry)

        cursor.close()
        self.conn.commit()

    def insert_item(self, item, backfill=False):
        if not item.is_valid:
            raise RuntimeError('Item is not valid. item={{{}}}.'.format(item))

        cursor = self.conn.cursor()

        type_id = self.get_or_insert_item_type(item.type)
        item_condition_id = models.item_conditions.get(item.item_condition, None)
        box_condition_id = models.box_conditions.get(item.box_condition, None)
        try:
            sale_status_id = models.sale_statuses[item.sale_status]
        except KeyError:
            raise KeyError('Invalid sale status. gcode={} sale_status={}'.format(item.gcode, item.sale_status))

        # Save into items
        # TODO change IGNORE?
        item_entry = (item.id, type_id, item.gcode_length, item.name, item.list_price, item.thumbnail_extension)
        cursor.execute('''
            INSERT OR IGNORE INTO items (
                id, type, gcode_length, name, list_price, thumbnail_extension
            )
            VALUES (?, ?, ?, ?, ?, ?)
        ''', item_entry)

        # Save into item_scodes
        current_datetime = self.get_insert_datetime(backfill=backfill)
        self.insert_or_update_item_scode(item.id, item.type, item.gcode_extra, current_datetime, backfill=backfill)

        # Save into item_snapshots
        new_item_entry = (item.id, type_id, item.gcode_extra, item.price,
                          item_condition_id, box_condition_id, item.release_date, sale_status_id, item.stock,
                          current_datetime)
        cursor.execute('''
            INSERT OR IGNORE INTO item_snapshots (
                id, type, gcode_extra, price, item_condition,
                box_condition, release_date, sale_status, stock, date
            )
            VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?)
        ''', new_item_entry)

        self.conn.commit()

        self.logger.info('Inserted item. gcode={} name={}'.format(item.gcode, item.name))
