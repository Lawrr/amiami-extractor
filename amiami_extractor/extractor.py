import datetime as dt
import logging
import time

from . import database, item_processor


# TODO rename gcodes to scodes
class Extractor:
    def __init__(self, db_path, update=False, backfill=False, full_run=False):
        self.db_manager = database.DatabaseManager(db_path)
        self.update = update
        self.backfill = backfill
        self.full_run = full_run
        self.logger = logging.getLogger(__name__)

    def close(self):
        self.db_manager.close()

    def run(self, query_urls, first_run_max_query_pages, max_query_pages, num_update_recent, num_update_old):
        is_first_run = self.db_manager.is_first_run()

        self.logger.info('Running extractor. is_first_run={}'.format(is_first_run))
        start = time.time()

        if is_first_run:
            max_query_pages = first_run_max_query_pages

        if self.update:
            self._extract_least_recently_checked_items(num_update_old)
            self._extract_recently_updated_items(num_update_recent)

        self._extract_new_items_from_urls(query_urls, max_query_pages, self.full_run)

        end = time.time()
        self.logger.info('Run completed [{0:.2f} secs elapsed]'.format(end - start))

    def _extract_new_items_from_urls(self, query_urls, max_query_pages, full_run):
        # TODO fix full_run
        pages_inserted = 0
        for url in query_urls:
            for page in range(1, max_query_pages + 1):
                new_items, unchanged_items = item_processor.get_new_items_from_url(url, start_page=page, end_page=page,
                                                                                   db_manager=self.db_manager, full_run=full_run)
                self._insert_items(new_items)
                self._update_last_checked(unchanged_items)
                pages_inserted += 1
                if pages_inserted % 25 == 0:
                    time.sleep(30)
                if len(new_items) == 0:
                    break

    def _extract_least_recently_checked_items(self, max):
        updated_items, unchanged_items = item_processor.get_updated_items_from_least_recently_checked(self.db_manager, max)
        self._insert_items(updated_items)
        self._update_last_checked(unchanged_items)

    def _extract_recently_updated_items(self, max):
        end = dt.datetime.now()

        for timedelta in [1, 3, 7]:
            start = end - dt.timedelta(days=timedelta)
            updated_items, unchanged_items = item_processor.get_updated_items_within_datetimes(start, end, self.db_manager, max)
            self._insert_items(updated_items)
            self._update_last_checked(unchanged_items)

    def _update_last_checked(self, items):
        insert_datetime = self.db_manager.get_insert_datetime(backfill=self.backfill)
        for item in items:
            self.db_manager.insert_or_update_item_scode(item.id, item.type, item.gcode_extra, insert_datetime, backfill=self.backfill)

    def _insert_items(self, items):
        for item in items:
            if item.sale_status is None:
                continue

            try:
                if item.thumbnail is not None:
                    saved = item.save_thumbnail()
                    if saved:
                        self.logger.debug('Saved new thumbnail. gcode={} path="{}"'.format(item.gcode, item.thumbnail_path))
                else:
                    self.logger.debug('Item thumbnail url is unknown - skipping thumbnail saving. gcode={}'.format(item.gcode))

                self.db_manager.insert_item(item, backfill=self.backfill)

            except Exception as e:
                # TODO rollback somewhere on failure?
                self.logger.exception('Error occurred saving item. gcode={}\n'.format(item.gcode) + repr(e))

                if item.thumbnail is not None:
                    item.delete_thumbnail()
