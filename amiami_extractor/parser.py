import logging
import re

from . import api, models

GCODE_MATCHER = re.compile(r'([\w\-]+?)-(\d+)(?:-(\w+))?')
PREOWNED_MATCHER = re.compile(r'\(Pre-owned ITEM:(.+?)/BOX:(.+?)\)')


def parse_search_items_json(json):
    items = []
    for item in json['items']:
        id, type, gcode_extra, gcode_length = parse_gcode(item['gcode'])
        name = item['gname']
        thumbnail_url = api.IMAGES_URL + item['thumb_url'].replace('/main/', '/thumb300/')

        if 'noimage' in thumbnail_url:
            thumbnail_url = None

        model = models.Item(id, type, gcode_extra, gcode_length, name, thumbnail_url)
        items.append(model)

    return items


def parse_item_details_json(json):
    if 'item' not in json:
        raise KeyError('Missing key "item" in json.')

    item = json['item']
    other_items = json['_embedded']['other_items']

    preowned = PREOWNED_MATCHER.match(item['sname'])
    if preowned:
        item_condition, box_condition = preowned.groups()
    else:
        item_condition = None
        box_condition = None

    # Fix inconsistent sale status for preorders
    item_status = item['salestatus']
    if item_status is None:
        sale_status_detail = item['salestatus_detail'] if 'salestatus_detail' in item else 'Not found'
        raise RuntimeError('Sale status is None. salestatus_detail={{{}}}'.format(sale_status_detail))
    if item_status.lower() == 'preorder':
        item_status = models.SaleStatus.Preorder.value

    details = {
        'scode': item['scode'],
        'list_price': item['list_price'],
        'price': item['price'],
        'item_condition': item_condition,
        'box_condition': box_condition,
        'release_date': item['releasedate'],
        'sale_status': item_status,
        'stock': item['stock'],
        'other_items': other_items
    }

    return details


def parse_other_items(item):
    logger = logging.getLogger(__name__)

    other_items = []
    for other_item_dict in item.other_items:
        other_gcode = other_item_dict['scode']
        id, type, gcode_extra, gcode_length = parse_gcode(other_gcode)

        if id != item.id or type != item.type or gcode_length != item.gcode_length:
            logger.warn('Other item does not match current item. gcode={} other_gcode={}'.format(item.gcode, other_gcode))
            continue

        other_item = models.Item(id, type, gcode_extra, gcode_length, item.name)
        other_item.thumbnail = item.thumbnail
        other_item.thumbnail_extension = item.thumbnail_extension

        other_items.append(other_item)

    return other_items


def parse_gcode(gcode):
    m = GCODE_MATCHER.match(gcode)
    if m:
        type, id, gcode_extra = m.groups()
        gcode_extra = '' if gcode_extra is None else gcode_extra
        gcode_length = len(id)
        id = int(id)
        return id, type, gcode_extra, gcode_length
    else:
        raise ValueError('Invalid product gcode. gcode={}'.format(gcode))
