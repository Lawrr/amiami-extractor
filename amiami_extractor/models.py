from enum import Enum
import io
import os

from PIL import Image

from . import constants, file

# Mappings are populated when the database loads/generates them
# String to int id
item_types = {}
item_conditions = {}
box_conditions = {}
sale_statuses = {}


class ItemCondition(Enum):
    S = 'S'
    A = 'A'
    AMinus = 'A-'
    BPlus = 'B+'
    B = 'B'
    C = 'C'
    J = 'J'


class BoxCondition(Enum):
    A = 'A'
    B = 'B'
    C = 'C'
    N = 'N'


class SaleStatus(Enum):
    Released = 'Released'
    Preorder = 'Pre-order'
    TentativePreorder = 'Tentative Pre-order'
    ProvisionalPreorder = 'Provisional Pre-order'
    Backorder = 'Back-order'


class Item:
    THUMBNAIL_SIZE = (128, 128)

    def __init__(self, id, type, gcode_extra, gcode_length, name, thumbnail_url=None):
        self.in_database = False

        self.id = id
        self.type = type
        self.gcode_extra = gcode_extra
        self.gcode_length = gcode_length
        self.name = name
        self.thumbnail_url = thumbnail_url

        self.list_price = None
        self.price = None
        self.item_condition = None
        self.box_condition = None
        self.release_date = None
        self.sale_status = None
        self.stock = None

        self.other_items = []

        self.thumbnail = None
        self.thumbnail_extension = None

    def __key(self):
        return (self.id, self.type, self.gcode_extra)

    def __eq__(self, other):
        return type(self) == type(other) and self.__key() == other.__key()

    def __hash__(self):
        return hash(self.__key())

    def set(self, **kwargs):
        self.__dict__.update((k, v) for k, v in kwargs.items())

    def __repr__(self):
        repr = '[{}] {}'.format(self.gcode, self.name)
        repr += '\n\tList price: ' + str(self.list_price) if self.list_price is not None else ''
        repr += '\n\tPrice: ' + str(self.price) if self.price is not None else ''
        repr += '\n\tItem condition: ' + self.item_condition if self.item_condition is not None else ''
        repr += '\n\tBox condition: ' + self.box_condition if self.box_condition is not None else ''
        repr += '\n\tRelease date: ' + self.release_date if self.release_date is not None else ''
        repr += '\n\tSale status: ' + self.sale_status if self.sale_status is not None else ''
        repr += '\n\tStock: ' + str(self.stock) if self.stock is not None else ''
        return repr

    @property
    def is_valid(self):
        if self.name is None:
            return False
        return True

    @property
    def gcode(self):
        if self.gcode_extra:
            gcode_extra = '-' + self.gcode_extra
        else:
            gcode_extra = self.gcode_extra

        return self.type + '-' + str(self.id).zfill(self.gcode_length) + gcode_extra

    @property
    def thumbnail_path(self):
        if self.thumbnail_extension is None:
            raise RuntimeError('Item has no thumbnail extension set')

        thumbnail_name = '{name}.{ext}'.format(name=self.id, ext=self.thumbnail_extension)
        subdir = str(self.id // constants.THUMBNAILS_PER_DIRECTORY)
        thumbnail_path = os.path.join(constants.IMAGES_PATH,
                                      file.clean_filename(self.type),
                                      subdir,
                                      file.clean_filename(thumbnail_name))
        return thumbnail_path

    def save_thumbnail(self):
        if self.thumbnail_extension is None:
            return False

        os.makedirs(os.path.dirname(self.thumbnail_path), exist_ok=True)
        if not os.path.exists(self.thumbnail_path):
            image = Image.open(io.BytesIO(self.thumbnail))
            image.thumbnail(self.THUMBNAIL_SIZE)
            image.save(self.thumbnail_path)
            return True

        return False

    def delete_thumbnail(self):
        if self.thumbnail_extension is None:
            return False

        if os.path.exists(self.thumbnail_path):
            os.remove(self.thumbnail_path)

            thumbnail_dir = os.path.dirname(self.thumbnail_path)
            if len(os.listdir(thumbnail_dir)) == 0:
                os.rmdir(thumbnail_dir)

            return True

        return False
