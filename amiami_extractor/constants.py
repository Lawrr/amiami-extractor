import __main__
import os

try:
    APP_DIR_PATH = os.path.abspath(os.path.dirname(__main__.__file__))
except AttributeError:
    APP_DIR_PATH = None

THUMBNAILS_PER_DIRECTORY = 10000

IMAGES_PATH = '{}/images/'.format(APP_DIR_PATH)
