from amiami_extractor import constants

APP_NAME = 'amiami-extractor'

QUERY_URLS = (
    'http://slist.amiami.com/top/search/list?s_cate2=459',   # Bishoujo Figures
    'http://slist.amiami.com/top/search/list?s_cate2=1298',  # Character Figures
    'http://slist.amiami.com/top/search/list?s_cate1=219',   # Trading Figures
    'http://slist.amiami.com/top/search/list?s_cate2=413',   # Character Goods
    'http://slist.amiami.com/top/search/list?s_cate2=10042', # Calendars
    'http://slist.amiami.com/top/search/list?s_cate2=161',   # Books/Mangas
    'http://slist.amiami.com/top/search/list?s_cate2=8547',  # Card Supplies
)

FIRST_RUN_MAX_QUERY_PAGES = 750
MAX_QUERY_PAGES = 50

NUM_UPDATE_RECENT = 120
NUM_UPDATE_OLD = 400

LOGGING_CONFIG_PATH = '{}/logging.json'.format(constants.APP_DIR_PATH)

DB_PATH = '{}/{}.sqlite3'.format(constants.APP_DIR_PATH, APP_NAME)
LOG_PATH = '{}/{}.log'.format(constants.APP_DIR_PATH, APP_NAME)
IMAGES_PATH = '{}/images/'.format(constants.APP_DIR_PATH)
